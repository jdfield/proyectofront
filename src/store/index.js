import Vue from 'vue'
import Vuex from 'vuex'
import initModule from './module/initModule'
import authModule from './module/authModule'
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    initModule,
    authModule
  },
  plugins: [createPersistedState()]
})
