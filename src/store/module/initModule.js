import { showUser } from '../../utils/init.api'

const state = {
    user: {},
    authenticated: !!window.localStorage.getItem('token'),
}

const getters = {
    getUser(state) {
        return state.users;
    }
}

const actions = {
    async show({ commit }) {
        try {
            const response = await showUser();
            console.log(response.data);
            commit('SET_ITEM', response.data.data);
        } catch (error) {
            // handle the error here
            console.log(error);
        }    
    },
    
}


const mutations = {
    SET_USER(state, data) {
        state.user = data;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}



