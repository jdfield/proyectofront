import { loginUser,registerUser } from '../../utils/init.api';

const state = {
    user: {},
    users: [],
    authenticated: !!window.localStorage.getItem('token'),
}

const getters = {
    getItems(state) {
        return state.items;
    },
    getUser(state) {
        return state.user;
    },
    getUsers(state) {
        return state.users;
    },
    isAuthenticated: state => {
       return state.authenticated
    }
}

const actions = {
    async fetchUsers({ commit }) {
            try {
                console.log('fetchUsers...');
                const response = await getAllUsers();
                commit('SET_USERS', response.data.data);
            } catch (error) {
                // handle the error here
                console.log(error);
            }    
        },

    async login({ commit }, data) {
            return loginUser(data.email,data.password).then(
                response => {
                    console.log(response.status);
                if (response.status == 200) {
                    
                    window.localStorage.setItem('token', response.data.accessToken);
                    console.log(state.authenticated);
                    commit('SET_AUTH', true);
                    
                }
                return Promise.resolve(response);
              },
              error => {
                return Promise.reject(error);
              }
            );
          },
        

     async register({ commit },data) {
            try {
                console.log("register  api");
                console.log(data);
                const response = await registerUser(data.user_email,data.user_name,data.user_password,data.user_user);
                console.log(response.data);
                if (response.data.status == 'Success') {
                    commit('ADD_USER', response.data.data);
                }
                
            } catch (error) {
                // handle the error here
                console.log(error);
            }    
        },

     async logout({ commit },data) {
            try {
                const response = await logout();
                if (response.data.status == 'Success') {
                    localStorage.removeItem('token');
                    commit('SET_USER', {});
                    commit('SET_AUTH', false);
                }
                
            } catch (error) {
                // handle the error here
                console.log(error);
            }    
        },
    async get({ commit }) {
            try {
                console.log('fetchUsers...');
                const response = await getAllUsers();
                commit('SET_USERS', response.data.data);
            } catch (error) {
                // handle the error here
                console.log(error);
            }    
        }
}


const mutations = {
    SET_USERS(state, data) {
        state.users = data;
    },
    SET_USER(state, data) {
        state.user = data;
    },
    SET_AUTH(state, data) {
        state.authenticated = data;
    },
    ADD_USER(state, data) {
        state.users.push(data);
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}



