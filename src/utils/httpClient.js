import axios from 'axios';

const httpClient = axios.create({
    baseURL: 'http://localhost:8080',
    timeout: 5000, // indicates, 5000ms ie. 5 seconds
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        // anything you want to add to the headers
    }
});


const getAuthToken = () => localStorage.getItem('token');

const authInterceptor = (config) => {
    config.headers['Authorization'] = 'Bearer '+getAuthToken();
    return config;
}

httpClient.interceptors.request.use(authInterceptor);

// interceptor to catch errors
const errorInterceptor = error => {
    // check if it's a server error
    if (!error.response) {
      
      return Promise.reject(error);
    }

    // all the other error responses
    switch(error.response.status) {
        case 400:
            console.error(error.response.status, error.message);
            
            break;

        case 401: // authentication error, logout the user
            
            //localStorage.removeItem('token');
            //router.push('/login');
            console.error(error.response);
            break;

        default:
            console.error(error.response.status, error.message);
            

    }
    return Promise.reject(error);
}

// Interceptor for responses
const responseInterceptor = response => {
    switch(response.status) {
        case 200: 
            // yay!
            break;
        // any other cases
        default:
            // default case
    }

    return response;
}

httpClient.interceptors.response.use(responseInterceptor, errorInterceptor);

export default httpClient;