import httpClient from './httpClient';

const showUser = () => httpClient.get('/editorials');
const loginUser = (usernameOrEmail, password) => httpClient.post('/api/auth/signin', { password, usernameOrEmail });

const registerUser = (email,name,password,username) => httpClient.post('/api/auth/signup', { email,name,password,username });


export {
    showUser,
    loginUser,
    registerUser
}
