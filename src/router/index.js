import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue';
import Principal from '../views/Principal.vue';
import Autor from '../components/AutorComponent.vue';
import Books from '../components/LibroComponent.vue';
import Editorial from '../components/EditorialComponent.vue';

Vue.use(VueRouter)
const ifNotAuthenticated = (to, from, next) => {
  // Si existe un token, la sesion existe, por lo cual, redirecciona a home
           if (window.localStorage.getItem('_token')) {
            console.log("inicio");
          //if (store.state.authModule.authenticated) {
            next({ path: '/' });
          } else {
            next();
          }
};
  const routes = [
    { 
      name: 'login',
      path: '/login', 
      component: Login,
      meta: { Auth: false, title: 'Login',
      authorize: [] },
      beforeEnter: ifNotAuthenticated
    },{ 
      name: 'autor',
      path: '/autor', 
      component: Autor,
      meta: { Auth: false, title: 'Autor'}
    },{ 
      name: 'editorial',
      path: '/editorial', 
      component: Editorial,
      meta: { Auth: false, title: 'Editorial'}
    },{ 
      name: 'register',
      path: '/register', 
      component: Register,
      meta: { Auth: false, title: 'Register'},
      beforeEnter: ifNotAuthenticated
    },{ 
      name: 'books',
      path: '/books', 
      component: Books,
      meta: { Auth: false, title: 'Books'},
      beforeEnter: ifNotAuthenticated
    },{ 
      name: 'welcome',
      path: '/', 
      component: Home,
      meta: { Auth: false, title: 'Welcome'},
      beforeEnter: ifNotAuthenticated
    },{ 
      name: 'principal',
      path: '/principal', 
      component: Principal,
      meta: { Auth: false, title: 'Principal'},
      beforeEnter: ifNotAuthenticated
    },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {



  const { authorize } = to.meta;
  
    if (to.meta.Auth) {
          if (!store.state.authModule.authenticated) {
              // not logged in so redirect to login page with the return url
              return next({ path: '/login',  returnUrl: to.path  });
          }
  
          // check if route is restricted by role
          if (authorize.length && !authorize.includes(store.getters['authModule/getUser'].type_user)) {
              // role not authorised so redirect to home page
              return next({ path: '/'});
          }
      }
      console.log("es mana");
      console.log(store.getters['authModule/isManager']);
      document.title = to.meta.title;
      next();
  
  
  });

export default router
